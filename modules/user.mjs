class User {
	#email;

	#password;

	constructor(email, password) {
		this.#email = email;
		this.#password = password;
	}

	get getPassword() {
		return this.#password;
	}

	get getEmail() {
		return this.#email;
	}
}

export default User;
