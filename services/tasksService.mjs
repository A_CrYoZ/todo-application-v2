import Task from "../modules/task.mjs";

class Tasks {
	// Make tasks property private, as we don't want to let manipulate it directly
	#tasks;

	constructor() {
		this.#tasks = [];
	}

	async addTask(taskTitle, taskDescription, creator, taskCompleted) {
		const taskFound = this.#tasks.find(
			(task) => task.description === taskDescription && task.creator === creator
		);
		if (!taskFound) {
			this.#tasks.push(
				new Task(taskTitle, taskDescription, creator, taskCompleted)
			);
			return Promise.resolve(true);
		}
		return Promise.resolve(false);
	}

	async completeTaskByTitle(taskTitle, creator) {
		const task = this.#tasks.find(
			(item) => item.title === taskTitle && item.creator === creator
		);

		if (task) {
			task.done = true;
			return Promise.resolve(true);
		}

		return Promise.resolve(false);
	}

	async getAllTasks(creator) {
		return Promise.resolve(
			this.#tasks.filter((task) => task.creator === creator)
		);
	}

	async getCompletedTasks(creator) {
		return Promise.resolve(
			this.#tasks
				.filter((task) => task.done && task.creator === creator)
				.sort((a, b) => a.id - b.id)
		);
	}

	async getIncompleteTasks(creator) {
		return Promise.resolve(
			this.#tasks
				.filter((task) => !task.done && task.creator === creator)
				.sort((a, b) => a.id - b.id)
		);
	}

	async updateTask(oldName, newTitle, newDescription, creator) {
		const task = await Promise.resolve(
			this.#tasks.find(
				(item) => item.title === oldName && item.creator === creator
			)
		);

		if (task) {
			task.title = newTitle;
			task.description = newDescription;

			return Promise.resolve(true);
		}

		return Promise.resolve(false);
	}

	async deleteTaskByTitle(title, creator) {
		const taskIndex = this.#tasks.findIndex(
			(task) => task.title === title && task.creator === creator
		);

		if (taskIndex !== -1) {
			this.#tasks.splice(taskIndex, 1);
			return Promise.resolve(true);
		}

		return Promise.resolve(false);
	}
}

export default Tasks;
