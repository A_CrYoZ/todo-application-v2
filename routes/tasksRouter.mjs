import { Router } from "express";
import createError from "http-errors";
import { check, validationResult, param } from "express-validator";

const router = new Router();

export default (tasks) => {
	// Set routes. I didn't create separate routers as there are only 5 routes
	router.get("/api/tasks/done", async (req, res, next) => {
		try {
			const { email } = req.user;

			const resTasks = await tasks.getCompletedTasks(email);

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks not found"));
			}
		} catch (error) {
			next(error);
		}
	});

	router.get("/api/tasks", async (req, res, next) => {
		try {
			const { email } = req.user;

			const resTasks = await tasks.getIncompleteTasks(email);

			if (resTasks) {
				res.json(resTasks);
			} else {
				next(createError(404, "Tasks not found"));
			}
		} catch (error) {
			next(createError(500, "Unable to read incomplete tasks"));
		}
	});

	router.post(
		"/api/tasks",
		[
			check("title")
				.trim()
				.isLength({ min: 3 })
				.escape()
				.withMessage("Minimal title length for task is 3 letter!"),
			check("description")
				.trim()
				.isLength({ min: 3 })
				.escape()
				.withMessage("Minimal description length for task is 3 letter!"),
		],
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { title, description } = req.body;

					const { email } = req.user;

					// Add task for user and if not exist - it will be automatically created
					const result = await tasks.addTask(title, description, email, false);

					if (result) {
						res.status(201).end();
					} else {
						next(createError(400, `Task ${description} already exists!`));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to add task"));
			}
		}
	);

	router.post(
		"/api/tasks/done",
		check("title").isLength({ min: 3 }).escape(),
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { title } = req.body;

					const { email } = req.user;

					const result = await tasks.completeTaskByTitle(title, email);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = await tasks.getIncompleteTasks(email);

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to complete task"));
			}
		}
	);

	router.put(
		"/api/tasks/:taskTitle",
		[
			param("taskTitle")
				.isLength({ min: 3 })
				.escape()
				.withMessage("taskTitle cannot be less than 3 symbols"),
			check("title")
				.isLength({ min: 3 })
				.escape()
				.withMessage("title cannot be less than 3 symbols"),
			check("description")
				.isLength({ min: 3 })
				.escape()
				.withMessage("description cannot be less than 3 symbols"),
		],
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { taskTitle } = req.params;
					const { title: newTitle, description: newDescription } = req.body;

					const { email } = req.user;

					const result = await tasks.updateTask(
						taskTitle,
						newTitle,
						newDescription,
						email
					);

					if (result) {
						res.json({ message: "ok" });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					let message = "";

					message = validationErrors
						.array()
						.reduce(
							(accumulator, currentValue) =>
								`${accumulator + currentValue.msg}. `,
							message
						);

					next(createError(400, message));
				}
			} catch (error) {
				next(createError(500, "Unable to complete task"));
			}
		}
	);

	router.delete(
		"/api/tasks",
		check("title")
			.isLength({ min: 3 })
			.escape()
			.withMessage("title cannot be less than 3 symbols"),
		async (req, res, next) => {
			try {
				const validationErrors = validationResult(req);

				if (validationErrors.isEmpty()) {
					const { title } = req.body;

					const { email } = req.user;

					const result = await tasks.deleteTaskByTitle(title, email);

					if (result) {
						// Get incomplete tasks for user to return to client
						const resTasks = tasks.getIncompleteTasks(email);

						res.json({ message: "ok", tasks: resTasks });
					} else {
						next(createError(404, "Couldn't find such task"));
					}
				} else {
					const errorMsg = validationErrors.array()[0].msg;
					next(createError(400, errorMsg));
				}
			} catch (error) {
				next(createError(500, "Unable to delete task"));
			}
		}
	);

	return router;
};
