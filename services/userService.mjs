import bcrypt from "bcrypt";

import User from "../modules/user.mjs";

const users = [];

class UserService {
	static async createUser(email, password) {
		const find = await this.findUser(email);

		if (find) throw new Error("The user already exist!");

		const hashedPassword = await bcrypt.hash(password, 10);

		const user = new User(email, hashedPassword);
		users.push(user);

		return user;
	}

	static async isValidPassword(user, password) {
		const compare = await bcrypt.compare(password, user.getPassword);

		return compare;
	}

	static async findUser(email) {
		return Promise.resolve(users.find((element) => element.getEmail === email));
	}
}

export default UserService;
