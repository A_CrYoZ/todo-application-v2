class Task {
	constructor(title, description, creator, done) {
		this.id = Date.now();
		this.title = title;
		this.description = description;
		this.done = done;
		this.creator = creator;
	}

	get getDescription() {
		return this.done;
	}

	set setDescription(description) {
		this.done = description;
	}

	get getCompleted() {
		return this.done;
	}

	set setCompleted(done) {
		this.done = done;
	}
}

export default Task;
