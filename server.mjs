import express from "express";

import TasksService from "./services/tasksService.mjs";
import tasksRouter from "./routes/tasksRouter.mjs";

import authRouter from "./routes/authRouter.mjs";

import passport from "./lib/auth.mjs";

const tasksService = new TasksService();

const app = express();
const port = 3001;

// Set body-parser
app.use(express.json());

// Set up passport
app.use(passport.initialize());

// Set up auth router
app.use("/api/auth", authRouter(passport));

// Authenticate users
app.use(passport.authenticate("jwt", { session: false }));

// Set up routers
app.use(tasksRouter(tasksService));

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
	const status = err.status ?? 500;

	if (err.status === 500) {
		console.error(err);
	} else {
		console.error(err.message);
	}

	res.status(status).send({ message: err.message });
});

app.listen(port, () => console.log(`API listening on port ${port}`));
